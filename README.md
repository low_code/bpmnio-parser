# BPMN.io Parser

The BPMN.io parser reads BPMN.io files and creates an object tree wich can be used for further processing.

## Developer Setup

For development the following setup is recommended:

* Visual Studio Code with the following plugins:
  * ms-python.python
  * vs-code-bpmn-io
* Python >= 3.8

Start developing:
* pip3 install -r requirements.txt

Run unit tests:
* python -m unittest discover tests/

Run a single test:
* python -m unittest tests.test_parser.Test_Parser.test_empty_class