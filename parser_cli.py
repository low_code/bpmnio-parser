#!/usr/bin/env python
import argparse

__version__ = '0.0.1'
__author__ = u'Michael Sieber'

def get_parser():
    """
    Creates a new argument parser.
    """
    parser = argparse.ArgumentParser('BPMN.io Parser')
    version = '%(prog)s ' + __version__
    parser.add_argument('--version', '-v', action='version', version=version)
    parser.add_argument('--file', '-f', type=argparse.FileType('r', encoding='UTF-8'),
                        required=True,
                        help='Specifies the BPMN.io file which should get parsed')
    return parser

def main(args=None):
    """
    Main entry point of the BPMN.io Parser.
    """

    argparser = get_parser()
    args = argparser.parse_args(args)


if __name__ == '__main__':
    main()
