from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='bpmnio-parser',
    version='0.0.1',
    description='Parser for BMPN.io diagrams',
    long_description=readme,
    author='Michael Sieber',
    url='https://gitlab.com/low_code/bpmnio-parser',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)